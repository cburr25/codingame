package thor

import math._
import scala.util._

/**
 * URL: https://www.codingame.com/ide/puzzle/power-of-thor-episode-1
 */

object Toolkit {
  def debug(s: String) = Console.err.println(s)
}

object Player extends App {
  import Toolkit._

  val Array(lightx, lighty, initialtx, initialty) = for (i <- readLine split " ") yield i.toInt
  val lightPosition = Position(lightx, lighty)
  val initialPosition = Position(initialtx, initialty)
  debug(s"Light = " + lightPosition)
  debug(s"Thor = " + initialPosition)

  getDirections(initialPosition, lightPosition)
    .foreach { direction =>
      val _ = readInt
      println(direction.name)
    }

  /*
  Helper models
   */

  case class Direction(
    name: String,
    xOp: Int => Int,
    yOp: Int => Int) {
    def merge(that: Direction) = Direction(
      name = name + that.name,
      xOp = xOp.compose(that.xOp),
      yOp = yOp.compose(that.yOp))
  }

  object Direction {
    val NoDirection = Direction("", identity, identity)
    val N = Direction("N", identity, _ - 1)
    val NE = Direction("NE", _ + 1, _ - 1)
    val E = Direction("E", _ + 1, identity)
    val SE = Direction("SE", _ + 1, _ + 1)
    val S = Direction("S", identity, _ + 1)
    val SW = Direction("SW", _ - 1, _ + 1)
    val W = Direction("W", _ - 1, identity)
    val NW = Direction("NW", _ - 1, _ - 1)
  }

  def getDirections(origin: Position, target: Position): Seq[Direction] = {
    @scala.annotation.tailrec
    def loop(origin: Position, target: Position, directions: Seq[Direction] = Seq.empty): Seq[Direction] = {
      debug(s"$origin going to $target | ${directions.length} moves so far")
      origin.getDirectionTo(target) match {
        case direction if direction.name == Direction.NoDirection.name => directions
        case direction => loop(origin.go(direction), target, direction +: directions)
      }
    }
    loop(origin, target)
  }

  case class Position(x: Int, y: Int) {
    def go(direction: Direction) = Position(
      x = direction.xOp(x),
      y = direction.yOp(y))

    def getDirectionTo(position: Position) = {
      val yDir = (this.y, position.y) match {
        case (y1, y2) if y1 > y2 => Direction.N
        case (y1, y2) if y1 < y2 => Direction.S
        case _ => Direction.NoDirection
      }
      val xDir = (this.x, position.x) match {
        case (x1, x2) if x1 > x2 => Direction.W
        case (x1, x2) if x1 < x2 => Direction.E
        case _ => Direction.NoDirection
      }
      yDir.merge(xDir)
    }
  }
}