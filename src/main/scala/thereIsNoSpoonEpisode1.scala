package nospoon

import math._
import scala.util._

//object Toolkit {
//  def debug(s: String) = Console.err.println(s)
//}

object Player extends App {
  //  import Toolkit._

  val width = readInt // the number of cells on the X axis
  val height = readInt // the number of cells on the Y axis

  case class Cell(x: Int, y: Int, isNode: Boolean)
  val emptyCell = Cell(-1, -1, isNode = false)

  // Draw grid as flattened cell sequence
  val cells: Seq[Cell] = (0 until height).flatMap { y =>
    readLine
      .toSeq
      .map(_ == '0') // isNode
      .zipWithIndex
      .map { case (isNode, x) => Cell(x, y, isNode) }
  }

  // Fold over the sequence and use pattern matching to accumulate nodes and their potential neighbors
  cells
    .foldLeft(Seq.empty[(Cell, Cell, Cell)]) {
      case (nodes, current @ Cell(x, y, isNode)) if isNode => {

        val nodeOrEmpty: Seq[Cell] => Cell = _.find(_.isNode).getOrElse(emptyCell)
        val right = nodeOrEmpty(cells.filter(c => c.y == y && c.x > x))
        val bottom = nodeOrEmpty(cells.filter(c => c.x == x && c.y > y))

        // Append node and its neighbors to accumulator
        nodes :+ ((current, right, bottom))
      }

      // Empty cells => return accumulator untouched
      case (nodes, _) => nodes
    }
    // Format into answer and print
    .map {
      case (Cell(x, y, _), Cell(rx, ry, _), Cell(bx, by, _)) =>
        s"$x $y $rx $ry $bx $by"
    }
    .foreach(println)
}