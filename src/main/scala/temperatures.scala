package temperatures

object Solution extends App {
  val n = readInt // the number of temperatures to analyse
  val temperatures = if (n == 0) Seq.empty else readLine.split(" ").map(_.toInt).toSeq

  val answer = temperatures.foldLeft(None: Option[Int]) {
    case (None, t) => Some(t)
    case (Some(closest), candidate) if candidate.abs == closest.abs && candidate > closest => Some(candidate)
    case (Some(closest), candidate) if candidate.abs < closest.abs => Some(candidate)
    case (best, _) => best
  }.getOrElse(0)

  println(answer)
}
