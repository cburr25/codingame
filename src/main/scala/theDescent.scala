package descent

object Toolkit {

  //  def debug(s: String) = Console.err.println(s)
}

object Player extends App {
  import Toolkit._

  while (true) {
    val x = (for (i <- 0 until 8) yield i -> readInt)
      .foldLeft(0 -> 0) {
        case (highest @ (_, highestHeight), current @ (_, currentHeight)) =>
          if (highestHeight > currentHeight)
            highest
          else current
      }
      ._1
    println(x)
  }
}
