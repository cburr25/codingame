import Command.PushInt
import Direction.{Direction, Down, Left, Right, Up}
import Grid.Grid
import GridInterpreter.State

//object Toolkit {
//  def debug(msg: String) = Console.err.println(msg)
//}
//import Toolkit._

/**
  In this solution, I try to only make use of immutable data structures and recursion.
  Output is collected and outputted only once the grid interpreter has completely traversed.
 */
object Solution extends App {
  val input = Seq.fill(readInt)(readLine).foldLeft(Seq.empty[String])(_ :+ _)
  val grid = Grid.fromInput(input)
  val result = GridInterpreter.traverseGrid(grid, State.initial)
  val output = result.output.reverse.mkString("")
  print(output)
}

object Grid {
  type Grid = Map[Position, Char]
  
  def fromInput(input: Seq[String]): Grid = {
    import scala.collection.breakOut
    val accumulator = (Position(0, 0), Map.empty: Grid)
    input.foldLeft(accumulator) {
      case ((position @ Position(_, y), finalGrid), line) =>
        val gridLine: Map[Position, Char] = line
          .zipWithIndex
          .map { case (char, x) => Position(x, y) -> char }(breakOut) // optimization, avoids intermediate collection
      
        val nextPos = position.copy(x = 0, y + 1)
        (nextPos, finalGrid ++ gridLine)
    }._2 // final grid
  }
}

object GridInterpreter {
  def traverseGrid(grid: Grid, initialState: State): State = {
    @scala.annotation.tailrec
    def loop0(grid: Grid, state: State): State = {
      if (!state.running) state
      else {
        val nextChar = grid(state.position)
        val command =
          if (state.isAsciiMode && nextChar != '"') PushInt(nextChar.toInt)
          else Command.fromChar(nextChar)
        loop0(grid, command(state).withNextPosition)
      }
    }
    loop0(grid, initialState)
  }

  case class State(
    direction: Direction,
    position: Position,
    stack: List[Int],
    isAsciiMode: Boolean,
    running: Boolean,
    output: List[String]
  ) {
    def withNextPosition: State = copy(position = direction match {
      case Up => position.copy(y = position.y - 1)
      case Down => position.copy(y = position.y + 1)
      case Right => position.copy(x = position.x + 1)
      case Left => position.copy(x = position.x - 1)
    })
  }

  object State {
    val initial = State(
      direction = Direction.Right,
      position = Position.initial,
      stack = List.empty,
      isAsciiMode = false,
      running = true,
      output = List.empty)
  }
}

case class Position(x: Int, y: Int)
object Position { val initial = Position(0, 0) }

object Direction {
  sealed trait Direction
  case object Up extends Direction
  case object Down extends Direction
  case object Right extends Direction
  case object Left extends Direction
}

object Command {
  def fromChar(char: Char): Command = char.toUpper match {
    case '+' => Addition
    case '*' => Multiplication
    case '-' => Subtraction
    case '>' => NewDirection(Direction.Right)
    case '<' => NewDirection(Direction.Left)
    case '^' => NewDirection(Direction.Up)
    case 'V' => NewDirection(Direction.Down)
    case 'E' => End
    case 'S' => SkipNext
    case 'P' => PopTop
    case 'X' => SwitchTopTwo
    case 'D' => DuplicateTop
    case '"' => ToggleStringMode
    case '_' => PopRightOrLeft
    case '|' => PopDownOrUp
    case 'I' => PopAndPrint
    case 'C' => PopAsciiPrint
    case ' ' => MoveOn
    case i if i.asDigit != -1 => PushInt(i.asDigit)
    case _ => throw new IllegalArgumentException(s"Could not parse char '$char' to command!")
  }

  // A command takes a State and returns a new, modified one.
  sealed trait Command {
    def apply(state: State): State
  }

  // No-op / blank space
  case object MoveOn extends Command {
    def apply(state: State): State = state
  }

  case object End extends Command {
    def apply(state: State): State = state.copy(running = false)
  }
  
  case object SkipNext extends Command {
    def apply(state: State): State = state.withNextPosition
  }

  case class NewDirection(newDirection: Direction) extends Command {
    def apply(state: State): State = state.copy(direction = newDirection)
  }

  case class PushInt(i: Int) extends Command {
    def apply(state: State): State = state.copy(stack = i :: state.stack)
  }

  case object Addition extends Command {
    def apply(state: State): State = {
      val a :: b :: tail = state.stack
      state.copy(stack = (a + b) :: tail)
    }
  }

  case object Subtraction extends Command {
    def apply(state: State): State = {
      val a :: b :: tail = state.stack
      state.copy(stack = (b - a) :: tail)
    }
  }

  case object Multiplication extends Command {
    def apply(state: State): State = {
      val a :: b :: tail = state.stack
      state.copy(stack = (a * b) :: tail)
    }
  }

  case object ToggleStringMode extends Command {
    def apply(state: State): State = state.copy(isAsciiMode = !state.isAsciiMode)
  }

  case object PopTop extends Command {
    def apply(state: State): State = state.copy(stack = state.stack.tail)
  }

  case object SwitchTopTwo extends Command {
    def apply(state: State): State = {
      val a :: b :: tail = state.stack
      state.copy(stack = b :: a :: tail)
    }
  }
  
  case object DuplicateTop extends Command {
    def apply(state: State): State = state.copy(stack = state.stack.head :: state.stack)
  }

  case object PopRightOrLeft extends Command {
    def apply(state: State): State = {
      val popped :: tail = state.stack
      val newDirection = if (popped == 0) Direction.Right else Direction.Left
      state.copy(
        stack = tail,
        direction = newDirection)
    }
  }

  case object PopDownOrUp extends Command {
    def apply(state: State): State = {
      val popped :: tail = state.stack
      val newDirection = if (popped == 0) Direction.Down else Direction.Up
      state.copy(
        direction = newDirection,
        stack = tail)
    }
  }

  case object PopAndPrint extends Command {
    def apply(state: State): State = {
      val popped :: tail = state.stack
      state.copy(
        stack = tail,
        output = popped.toString :: state.output)
    }
  }

  case object PopAsciiPrint extends Command {
    def apply(state: State): State = {
      val popped :: tail = state.stack
      state.copy(
        stack = tail,
        output = popped.toChar.toString :: state.output)
    }
  }
}
