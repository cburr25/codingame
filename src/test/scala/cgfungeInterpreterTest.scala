package cgfunge

import Grid.Position
import cgfunge.GridInterpreter.State
import org.scalatest.{ FlatSpec, Matchers }

class SolutionTest extends FlatSpec with Matchers {
  import Command._

  /*private val example2 = Seq(
    "vE<",
    "1 +",
    ">1^"
  )



  private val example3 = Seq("SE S+1<")*/

  /*private val ex2 = Seq(
    "1",
    "2419+*+IE"
  )
  private val res2 = 42*/

  "Grid Builder" should "build valid Grid from input" in {
    // Given fixtures
    val input = Seq("11 + E")
    // When
    val result = Grid.fromLines(input)
    // Then
    result shouldEqual Map(
      Position(0, 0) -> '1',
      Position(1, 0) -> '1',
      Position(2, 0) -> ' ',
      Position(3, 0) -> '+',
      Position(4, 0) -> ' ',
      Position(5, 0) -> 'E')
  }

  "Command" should "return new interpreter state" in {
    // Given
    val command = PushInt(1)
    val initState = State.initial

    // When
    val newState = command(initState)

    // Then
    val expected = initState.copy(stack = List(1), position = Position(1, 0))
    newState shouldBe expected
  }

  "SkipNext" should "Skip the next character and continue with the subsequent character" in {
    // Given
    val state = State.initial.copy(position = Position(y = 1, x = 2), direction = Direction.Down)

    // When
    val result = SkipNext(state)

    // Then
    result.position shouldBe Position(y = 3, x = 2)
  }

  "End" should "End the program immediately" in {
    // Given
    val state = State.initial

    // When
    val result = End(state)

    // Then
    result.running shouldBe false
  }

  "NewDirection" should "change the reading direction of the program" in {
    // Given
    val state = State.initial.copy(direction = Direction.Left)

    // When
    val result = Command.NewDirection(Direction.Right)(state)

    // Then
    result.direction shouldBe Direction.Right
  }

  "Addition" should "pop two integers and push result to stack" in {
    // Given
    val state = State.initial.copy(stack = List(1, 2))

    // When
    val result = Addition(state)

    // Then
    result.stack shouldBe List(3)
  }

  "Subtraction" should "pop two integers and push result to stack" in {
    // Given
    val state = State.initial.copy(stack = 4 :: 3 :: Nil)

    // When
    val result = Subtraction(state)

    // Then
    result.stack shouldBe List(1)
  }

  "Multiplication" should "pop two integers and push result to stack" in {
    // Given
    val state = State.initial.copy(stack = List(1, 2))

    // When
    val result = Multiplication(state)

    // Then
    result.stack shouldBe List(2)
  }

  "PopRightOrLeft" should "Pop the top value from the stack. If it is 0, continue to the right. Otherwise, go left." in {
    // Given
    val state = State.initial.copy(stack = List(1))

    // When
    val result = PopRightOrLeft(state)

    // Then
    result.direction shouldBe Direction.Left
  }

  "PopDownOrUp" should "Pop the top value from the stack. If it is 0, continue down. Otherwise, go up." in {
    // Given
    val state = State.initial.copy(stack = List(0))

    // When
    val result = PopDownOrUp(state)

    // Then
    result.direction shouldBe Direction.Down
  }

  "SwitchTopTwo" should "Switch the order of the top two stack values" in {
    // Given
    val state = State.initial.copy(stack = List(1, 2))

    // When
    val result = SwitchTopTwo(state)

    // Then
    result.stack shouldBe List(2, 1)
  }

  "DuplicateTop" should "Push a duplicate of the top value onto the stack" in {
    // Given
    val state = State.initial.copy(stack = List(1, 2))

    // When
    val result = DuplicateTop(state)

    // Then
    result.stack shouldBe List(1, 1, 2)
  }

  "ToggleStringMode" should "toggle ASCII mode on and off" in {
    // Given
    val state = State.initial

    // When
    val on = ToggleStringMode(state)
    val off = ToggleStringMode(on)

    // Then
    on.isAsciiMode shouldBe true
    off.isAsciiMode shouldBe false
  }

  "Grid Interpreter" should "apply multiple commands to state" in {
    // Given
    val commands = Seq(
      PushInt(1),
      SkipNext,
      PushInt(2))
    val initState = State.initial

    // When
    val result = commands.foldLeft(initState) {
      case (state, command) => command(state)
    }

    // Then
    val expected =
      State.initial.copy(
        direction = Direction.Right,
        position = Position(4, 0),
        stack = List(2, 1))

    result shouldBe expected
  }

  "PopTop" should "Pop the top value" in {
    // Given
    val state = State.initial.copy(stack = List(1))

    // When
    val result = PopTop(state)

    // Then
    result.stack shouldBe Nil
  }

  "PopAndPrint" should "Pop the top integer from the stack and print it to stdout." in {
    // Given
    val state = State.initial.copy(stack = List(1, 2))
    // When
    val result = PopAndPrint(state)
    // Then
    result.output.lastOption shouldBe Some("1")
  }

  "PopAsciiPrint" should "Pop the top integer from the stack and interpret it as an ASCII code, printing the corresponding character to stdout" in {
    // Given
    val state = State.initial.copy(stack = List(49))
    // When
    val result = PopAsciiPrint(state)
    // Then
    result.output.lastOption shouldBe Some("1")
  }

  "Grid interpreter" should "parse Commands from Chars" in {
    // Given
    val chars = "11+E"
    val expected = Seq(PushInt(1), PushInt(1), Addition, End)
    // When
    val result = chars.map(Command.fromChar)
    // Then
    result shouldBe expected
  }

  // TODO: separate navigation testing, this test implicitly tests ASCII mode and position update
  it should "push ASCII characters codes instead of parsing commands while in ASCII mode" in {
    // Given
    val state0 = State.initial.copy(isAsciiMode = true)
    val grid = Grid.fromLines(Seq("11+E"))
    val runNext = GridInterpreter.runNext(grid) _

    // When
    val state1: State = runNext(state0)
    val state2 = runNext(state1)
    val state3 = runNext(state2)
    val state4 = runNext(state3)

    // Then
    state1 shouldBe state0.copy(position = Position(1, 0), stack = List(49))
    state2 shouldBe state1.copy(position = Position(2, 0), stack = List(49, 49))
    state3 shouldBe state2.copy(position = Position(3, 0), stack = List(43, 49, 49))
    state4 shouldBe state3.copy(position = Position(4, 0), stack = List(69, 43, 49, 49))
  }

  it should "navigate grid and get output" in {
    // Given
    val state = State.initial
    val grid = Grid.fromLines(Seq("11+IE"))
    val interpreter = GridInterpreter(grid, state)

    // When
    val result = interpreter.traverseGrid()

    // Then
    result.stack shouldBe Nil // was popped!
    result.output shouldBe Seq("2")
  }

  // TODO didnt test multiline input

  // TODO didnt test toggle string mode, '"' must end it !

  // TODO test popandprint !!

  // TODO faild new direction test !!

  // TODO grild build test failed to find cases !!

  // TODO grid build test failed to test upper/lower

  // TOO MANY FAILS

  // TODO research Traversable

  val test = Seq(
    """39DD1+*+DI  >11091+   v>v""",
    """v  " on the wall"    < D """,
    """>>     "reeb fo"      v S""",
    """0v<" bottles "        < C""",
    """X>DSC_SvPD      _25*+Cv |""",
    """       *v   IS        < P""",
    """^IDX-1 <>  SC        0v X""",
    """v   "pass it around"  < 1""",
    """>    " ,nwod eno ekat" ^-""",
    """ Sing it!   ^+55D-1X_ESD<""")

  val res =
    """
      |99 bottles of beer on the wall
      |99 bottles of beer
      |take one down, pass it around
      |98 bottles of beer on the wall
      |98 bottles of beer
      |take one down, pass it around
      |97 bottles of beer on the wall
      |97 bottles of beer
      |take one down, pass it around
    """.stripMargin

  "Solution 5" should "bla" in {
    // Given
    val input = test
    val grid = Grid.fromLines(input)
    val init = State.initial
    val interpreter = GridInterpreter(grid, init)
    // When
    val result = interpreter.traverseGrid()
    // Then
    result.output.mkString("") shouldBe res
  }
}