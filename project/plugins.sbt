addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.1")

addSbtPlugin("com.sksamuel.scapegoat" %% "sbt-scapegoat" % "1.0.8")

addSbtPlugin("org.scalariform" % "sbt-scalariform" % "1.8.2")

//addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.10.2")
