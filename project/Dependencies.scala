import sbt._

object Dependencies {
  
  object scalatest {
    val version = "3.0.5"
    
    val scalactic = "org.scalactic" %% "scalactic" % version
    val scalatest = "org.scalatest" %% "scalatest" % version
  }
}