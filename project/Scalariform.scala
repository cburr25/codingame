// enforce minimum required syntax style
import sbt.Keys._
import sbt._

object Scalariform {
  import scalariform.formatter.preferences._
  import com.typesafe.sbt.SbtScalariform.ScalariformKeys
  
  lazy val settings = Seq(
    ScalariformKeys.preferences := ScalariformKeys.preferences.value.
      setPreference(AlignSingleLineCaseStatements, false).
      setPreference(CompactControlReadability, false).
      setPreference(CompactStringConcatenation, false).
      setPreference(DoubleIndentConstructorArguments, false).
      setPreference(FormatXml, true).
      setPreference(IndentLocalDefs, false).
      setPreference(IndentPackageBlocks, true).
      setPreference(IndentSpaces, 2).
      setPreference(MultilineScaladocCommentsStartOnFirstLine, false).
      setPreference(PreserveSpaceBeforeArguments, false).
      setPreference(RewriteArrowSymbols, false).
      setPreference(SpaceBeforeColon, false).
      setPreference(SpaceInsideBrackets, false).
      setPreference(SpacesAroundMultiImports, true).
      setPreference(SpacesWithinPatternBinders, true)
  )
}
