val commonSettings = Scalac.settings ++ Scalariform.settings ++ Scapegoat.settings

resolvers +=
  "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"

resolvers += Resolver.sonatypeRepo("releases")

lazy val root = (project in file(".")).
  enablePlugins(JavaAppPackaging).
  settings(inThisBuild(List(
      organization := "cbu",
      scalaVersion := "2.12.4"
    )) ++ Seq(
      name := "codingame",
      libraryDependencies ++= Seq(
        "ch.qos.logback" % "logback-classic" % "1.2.3",
        // Test dependencies
        Dependencies.scalatest.scalactic,
        Dependencies.scalatest.scalatest % Test
      )
  ) ++ commonSettings)
